import Image from 'next/image'
import { Inter } from 'next/font/google'
import HomepageView from '@/src/views/HomepageView'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <HomepageView/>
  )
}
