import Link from 'next/link'
import React from 'react'

function Header() {
  return (
    <div
    className='w-full px-12 py-4 bg-white flex justify-between items-center'
      >
        <div
        className='flex flex-row space-x-3'
        >
            <div
            className='w-7 h-7 bg-black rounded-full'
            >

            </div>
        </div>
        <nav
        className=''
        >
            <ul
            className='flex space-x-3 divide-black items-center'
            >
                <li
                className='pl-3 text-s font-medium'
                >
                    <Link
                    href={'/'}
                    className='hover:underline'
                    >
                    <span>
                        {`Nouveautés`}
                    </span>
                    </Link>
                </li>
                <li
                className='pl-3 text-s font-medium'
                >
                    <Link
                    href={'/'}
                    className='hover:underline'
                    >
                    <span>
                        {`Homme`}
                    </span>
                    </Link>
                </li>
                <li
                className='pl-3 text-s font-medium'
                >
                    <Link
                    href={'/'}
                    className='hover:underline'
                    >
                    <span>
                        {`Femme`}
                    </span>
                    </Link>
                </li>
                <li
                className='pl-3 text-s font-medium'
                >
                    <Link
                    href={'/'}
                    className='hover:underline'
                    >
                    <span>
                        {`Enfant`}
                    </span>
                    </Link>
                </li>
                <li
                className='pl-3 text-s font-medium'
                >
                    <Link
                    href={'/'}
                    className='hover:underline'
                    >
                    <span>
                        {`Offres`}
                    </span>
                    </Link>
                </li>
            </ul>
        </nav>
        <div
        className='flex flex-row space-x-3 justify-end items-center'
        >
            <div
            className='w-40 h-10 bg-black/5 rounded-full flex justify-center items-center'
            >
                <div
                className='w-7 h-7 rounded-full hover:bg-black/10 hover:cursor-pointer'
                >

                </div>
                <div
                className='placeholder:text-sm placeholder:text-black flex justify-center items-center'
                >
                    {'Rechercher'}
                </div>
                
            </div>
            <div
            className='w-7 h-7 bg-black rounded-full'
            ></div>
            <div
            className='w-7 h-7 bg-black rounded-full'
            ></div>
        </div>
      
    </div>
  )
}

export default Header
